# README #

### What is this repository for? ###

This repo contains Postman Collections v2.1 for testing ASCi TMO Work Requests Management API. Together they make up a test harness.

### How do I get set up? ###

* Import collections into Postman by pointing Postman to a folder with all the JSON files in this repo.
* Set environment variables to match URL of where API is hosted
* Configure base authentication on a collection level for each collection of requests.
* Run requests inside the collections one by one. Note that you can (and should) modify query parameters and payloads as necessary to suit a specific variant of test data you'd like to use.

### Who do I talk to? ###

* Viktoriia Kuznetcova (viktoriia.kuznetcova@chorus.co.nz)
* Raj Natalia (ASCi test manager, raj.natalia@chorus.co.nz) or Tim Clephane (Work Requests Management API dev, tim.clephane@chorus.co.nz)